# Chrooted OpenSSHD

This role has one mode of operation, it creates a separate chrooted OpenSSH on your system. The idea is that there should always be a non-chrooted ssh available for admins.

You can choose the name of that environment by setting ``chroot_name`` variable on playbook runtime. So you can have multiple chrooted environments per node.

## OS Support

Made with CentOS and Fedora in mind but you can support other distros by setting the following variables.

* ``sshd_confdir`` - default: /etc/ssh
* ``sshd_sbin`` - default: /usr/sbin/sshd
