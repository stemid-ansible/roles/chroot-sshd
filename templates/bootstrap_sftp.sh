#!/usr/bin/env bash

home_base=/var/sftp

test $(id -u $PAM_USER) -gt 500 || exit 1
grep ^{{ chroot_allowgroups }} /etc/group | grep -q $PAM_USER || exit 1

_HOME=$(getent passwd $PAM_USER 2>/dev/null | cut -d: -f6 2>/dev/null)

if [ -z "$_HOME" ]; then
  _HOME="/home/$PAM_USER"
fi

test -d $_HOME || exit 1

if [ ! -d "$home_base/$PAM_USER" ]; then
  echo "Creating home $home_base/$PAM_USER"
  mkdir -p "$home_base/$PAM_USER"
fi

if [ ! -d "$home_base/$PAM_USER/home/$PAM_USER" ]; then
  echo "Creating home mount point for $PAM_USER"
  mkdir -p "$home_base/$PAM_USER/home/$PAM_USER"
fi

if ! mount | grep "$home_base/$PAM_USER/home/$PAM_USER type" &>/dev/null; then
  echo "Mounting home in users ($PAM_USER) sftp-dir "
  mount -B "$_HOME" "$home_base/$PAM_USER/home/$PAM_USER"
fi

